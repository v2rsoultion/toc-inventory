import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RecordsTableComponent } from "src/app/_layout/records-table/records-table.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { ChartistModule } from "ng-chartist";
import { FormsModule } from "@angular/forms";
import { UiSwitchModule } from "ngx-ui-switch";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DataTablesModule } from "angular-datatables";

@NgModule({
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    UiSwitchModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgbModule,
    DataTablesModule,
  ],
  declarations: [RecordsTableComponent],
  exports: [RecordsTableComponent],
})
export class RecordsTableModule {}
