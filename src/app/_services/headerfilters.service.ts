import { BehaviorSubject } from "rxjs";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class HeaderfiltersService {
  channeldataUrl = environment.apiUrl + "/TOCPost/Getchanneldata"; // (Post Api) All Channel list in Array
  patnerdataUrl = environment.apiUrl + "/TOCPost/Getpartnersdata"; // (Post Api) All region list in Array

  getregiondataUrl = environment.apiUrl + "/TOCPost/Getregiondata"; // (Post Api) All state list in Array
  getstatedataUrl = environment.apiUrl + "/TOCPost/Getstatedata"; // (Post Api) All Location list in Array
  getLocationUrl = environment.apiUrl + "/TOCPost/Getcitydata"; // (Post Api) All Channel list in Array
  getStoreUrl = environment.apiUrl + "/TOCPost/Getstoresdata"; // (Post Api) All Channel list in Array
  getBrandUrl = environment.apiUrl + "/TOCPost/Getlocationbrandsdata"; // (Post Api) All Channel list in Array

  getProductUrl = environment.apiUrl + "/TOCPost/Getproductbranddata"; // (Post Api) All Getproductbranddata list in Array
  getGetgenderUrl = environment.apiUrl + "/TOCPost/Getgenderdata"; // (Post Api) All Getgenderdata list in Array
  GetcategorydataUrl = environment.apiUrl + "/TOCPost/Getcategorydata"; // (Post Api) All Getcategorydata list in Array
  getSubcategorydataUrl = environment.apiUrl + "/TOCPost/Getsubcategorydata"; // (Post Api) All Getcategorydata list in Array
  gettypedataUrl = environment.apiUrl + "/TOCPost/Gettypedata"; // (Post Api) All Gettypedata list in Array
  getlastnamedataUrl = environment.apiUrl + "/TOCPost/Getlastnamedata";
  getseasondataUrl = environment.apiUrl + "/TOCPost/Getseasondata";
  getmrpdataUrl = environment.apiUrl + "/TOCPost/Getmrpdata";
  getarticalnodataUrl = environment.apiUrl + "/TOCPost/Getarticalnodata";

  callFilterData = new BehaviorSubject(false);
  callFilterDataRef = this.callFilterData.asObservable();

  selectedChannelDatalist = [];
  channelDataListn = [];
  selectedpartnerDatalist = [];
  selectedregionDatalist = [];
  selectedStateDatalist = [];
  selectedLocationDatalist = [];
  selectedStoreCodeDatalist = [];
  selectedBrandsDatalist = [];
  productBrandDataDatalist = [];
  genderDatalist = [];
  categoryDataList = [];
  subCategoryDatalist = [];
  typeDatalist = [];
  lastNameDatalist = [];
  seasonDatalist = [];
  mrpDatalist = [];
  articalNumberDatalist = [];

  constructor(private http: HttpClient) {}
  getChannel(apiParams?: any) {
    return this.http.post(this.channeldataUrl, apiParams);
  }

  getPatners(apiParams?: any) {
    return this.http.post(this.patnerdataUrl, apiParams);
  }

  Getregiondata(apiParams?: any) {
    return this.http.post(this.getregiondataUrl, apiParams);
  }

  getStates(apiParams?: any) {
    return this.http.post(this.getstatedataUrl, apiParams);
  }
  getLocation(apiParams?: any) {
    return this.http.post(this.getLocationUrl, apiParams);
  }

  getStore(apiParams?: any) {
    return this.http.post(this.getStoreUrl, apiParams);
  }
  getBrands(apiParams?: any) {
    return this.http.post(this.getBrandUrl, apiParams);
  }

  //Product Fillter
  getPoducts(apiParams?: any) {
    return this.http.post(this.getProductUrl, apiParams);
  }

  getGenderData(apiParams?: any) {
    return this.http.post(this.getGetgenderUrl, apiParams);
  }

  getCategoryData(apiParams?: any) {
    return this.http.post(this.GetcategorydataUrl, apiParams);
  }

  getSubcategorydata(apiParams?: any) {
    return this.http.post(this.getSubcategorydataUrl, apiParams);
  }

  getTypedata(apiParams?: any) {
    return this.http.post(this.gettypedataUrl, apiParams);
  }

  getLastnamedata(apiParams?: any) {
    return this.http.post(this.getlastnamedataUrl, apiParams);
  }

  getSeasondataList(apiParams?: any) {
    return this.http.post(this.getseasondataUrl, apiParams);
  }

  getMrpdatalist(apiParams?: any) {
    return this.http.post(this.getmrpdataUrl, apiParams);
  }
  getarticalnoData(apiParams?: any) {
    return this.http.post(this.getarticalnodataUrl, apiParams);
  }

  getFilters(params?: any) {
    return this.http.post(environment.apiUrl + "/TOCPost/GetFilter", params);
  }

  saveFilter(params?: any) {
    return this.http.post(environment.apiUrl + "/TOCPost/PostFilter", params);
  }

  deleteFilter(params?: any) {
    return this.http.post(environment.apiUrl + "/TOCPost/deleteFilter", params);
  }
}
