import { environment } from "./../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class DepthService {
  GetDepthdataUrl = environment.apiUrl + "/TOCPost/GetDepthdata"; // (Post Api) All GetIbttdata list in Array
  GetDepthChartDataUrl = environment.apiUrl + "/TOCPost/Getdepthchartdata"; // (Post Api) All GetIbttdata list in Array
  constructor(private http: HttpClient) {}

  getDepthdata(apiParams?: any) {
    return this.http.post(this.GetDepthdataUrl, apiParams);
  }

  getDepthChartData(apiParams?: any) {
    return this.http.post(this.GetDepthChartDataUrl, apiParams);
  }
}
