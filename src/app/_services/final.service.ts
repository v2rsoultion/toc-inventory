import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class FinalService {
  GetFinlListUrl = environment.apiUrl + "/TOCPost/GetFinalrecommend"; // (Post Api) All GetIbttdata list in Array
  constructor(private http: HttpClient) {}

  getFinalList(apiParams?: any) {
    return this.http.post(this.GetFinlListUrl, apiParams);
  }
}
