import { BehaviorSubject } from 'rxjs';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  getdashboarddataUrl = environment.apiUrl + '/TOCPost/Getdashboarddata';
  getCurrentStatusUrl = environment.apiUrl + '/TOCPost/GetCurrentStatus'; // (Post Api) All Dashboard list in Array
  constructor(private http: HttpClient) { }
  pendingDecisions=new BehaviorSubject({});
  pendingDecisionsRef = this.pendingDecisions.asObservable();

  getdashboarddata(apiParams?: any) {
    return this.http.post(this.getdashboarddataUrl, apiParams);
  }

  GetCurrentStatus(apiParams?: any) {
    return this.http.post(this.getCurrentStatusUrl, apiParams);
  }
}
