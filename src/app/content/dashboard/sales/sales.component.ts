import { Title } from "@angular/platform-browser";
import { DatePipe } from "@angular/common";
import { DashboardService } from "./../../../_services/dashboard.service";
import { HeaderfiltersService } from "./../../../_services/headerfilters.service";
import { Component, OnInit, ViewChild, Renderer2 } from "@angular/core";
import * as Chartist from "chartist";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { ChartEvent, ChartType } from "ng-chartist";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarDirective,
  PerfectScrollbarConfigInterface,
} from "ngx-perfect-scrollbar";
import { ChartApiService } from "../../../_services/chart.api";
const selectData = require("../../../../assets/data/forms/form-elements/select.json");

import { Router } from "@angular/router";
import { NgSelectDataService } from "src/app/_services/ng-select-data.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}
@Component({
  selector: "app-sales",
  templateUrl: "./sales.component.html",
  styleUrls: ["./sales.component.css"],
})
export class SalesComponent implements OnInit {
  @ViewChild("ng-select") ngSelect;
  @BlockUI("revenue") blockUIRevenue: NgBlockUI;
  @BlockUI("hitrate") blockUIHitRate: NgBlockUI;
  @BlockUI("email") blockUIEmail: NgBlockUI;
  /**
   * lineChart
   */
  public lineChartsData = [
    { data: [], label: "" },
    { data: [], label: "" },
    { data: [], label: "" },
    { data: [], label: "" },
    { data: [], label: "" },
  ];
  public lineChartsData2 = [
    {
      data: [],
      label: "",
    },
  ];

  selectedChannelDatalist = [];
  selectedpartnerDatalist = [];
  selectedregionDatalist = [];
  selectedStateDatalist = [];
  selectedLocationDatalist = [];
  selectedStoreCodeDatalist = [];
  selectedBrandsDatalist = [];

  //Product tab Selected data
  productBrandDataDatalist = [];
  genderDatalist = [];
  categoryDataList = [];
  subCategoryDatalist = [];
  typeDatalist = [];
  lastNameDatalist = [];
  seasonDatalist = [];
  mrpDatalist = [];
  articalNumberDatalist = [];
  filterJson: any = "";
  typeDepth: String = "Depth";
  typeSales: String = "Sales";
  public lineChartsLabels = lineChartsLabels;
  public lineChartsLabels2 = lineChartsLabels2;
  public lineChartsOptions = lineChartsOptions;
  public lineChartsColors = lineChartsColors;
  public lineChartsLegend = lineChartsLegend;
  public lineChartsType = lineChartsType;
  getCurrentStatusData: any;
  currentstatussummarysales = [];
  dashboarddata: any;
  public config: PerfectScrollbarConfigInterface = { suppressScrollY: true };
  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  salesData: any;
  lineArea: any;
  earningchart: any;
  donutChart2: any;
  donutChart1: any;
  public breadcrumb: any;
  options = {
    bodyClass: ["pt-0"],
    close: false,
    expand: false,
    minimize: false,
    reload: true,
  };
  public singleSelectArray = selectData.singleSelectArray;
  singlebasicSelected: any = "Yesterday";
  currentstatussummaryDepth: any;
  hitRateOptions = {
    bodyClass: ["bg-hexagons", "pt-0"],
    headerClass: ["bg-hexagons"],
    cardClass: ["pull-up"],
    close: false,
    expand: false,
    minimize: false,
    reload: true,
  };

  dealsOptions = {
    bodyClass: ["bg-hexagons-danger"],
    cardClass: ["pull-up"],
    contentClass: ["bg-gradient-directional-danger"],
  };

  emailsOptions = {
    bodyClass: ["pt-0"],
    close: false,
    expand: false,
    minimize: false,
    reload: true,
  };
  loadingIndicator = true;

  firstRow = [
    "../../../assets/images/portrait/small/avatar-s-4.png",
    "../../../assets/images/portrait/small/avatar-s-5.png",
    "../../../assets/images/portrait/small/avatar-s-6.png",
  ];
  secondRow = [
    "../../../assets/images/portrait/small/avatar-s-7.png",
    "../../../assets/images/portrait/small/avatar-s-8.png",
  ];
  thirdRow = [
    "../../../assets/images/portrait/small/avatar-s-1.png",
    "../../../assets/images/portrait/small/avatar-s-2.png",
    "../../../assets/images/portrait/small/avatar-s-3.png",
  ];
  fourthRow = [
    "../../../assets/images/portrait/small/avatar-s-11.png",
    "../../../assets/images/portrait/small/avatar-s-12.png",
  ];
  fifthRow = [
    "../../../assets/images/portrait/small/avatar-s-6.png",
    "../../../assets/images/portrait/small/avatar-s-4.png",
  ];
  rows = [
    {
      type: "danger",
      value: 85,
      product: "iPhone X",
      image: this.firstRow,
      buttonname: "Mobile",
      amount: "$ 1200.00",
      bagde: "+8 more",
    },
    {
      type: "success",
      value: 75,
      product: "iPad",
      image: this.secondRow,
      buttonname: "Teblet",
      amount: "$ 1190.00",
      bagde: "+5 more",
    },
    {
      type: "danger",
      value: 65,
      product: "OnePlus",
      image: this.thirdRow,
      buttonname: "Mobile",
      amount: "$ 999.00",
      bagde: "+3 more",
    },
    {
      type: "success",
      value: 55,
      product: "ZenPad",
      image: this.fourthRow,
      buttonname: "Teblet",
      amount: "$ 1150.00",
    },
    {
      type: "danger",
      value: 45,
      product: "Pixel 2",
      image: this.fifthRow,
      buttonname: "Mobile",
      amount: "$ 1180.00",
    },
  ];

  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;
  Daygraph = true;
  Weekgraph = false;
  Monthgraph = false;
  ngOnInit() {
    this.titleService.setTitle("TOC-Inventory Dashborad");
    this.getdashboarddata();
    this.getCurrentStatus();
    // this.chartApiservice.getSalesData().subscribe((Response) => {
    //   this.salesData = Response;
    //   this.getChartdata();
    // });
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: false,
          link: "/dashboard/sales",
        },
      ],
    };
    this.headerService.callFilterDataRef.subscribe((res) => {
      if (res) {
        this.getdashboarddata();
        this.getCurrentStatus();
        this.headerService.callFilterData.next(false);
      }
    });
  }
  constructor(
    private modalService: NgbModal,
    private _renderer: Renderer2,
    private route: Router,
    private chartApiservice: ChartApiService,
    private dataService: NgSelectDataService,
    private headerFillters: HeaderfiltersService,
    private dashboardservice: DashboardService,
    private datePipe: DatePipe,
    private titleService: Title,
    private headerService: HeaderfiltersService
  ) {}
  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown filters-modal",
      size: "lg",
    });
  }
  reloadRevenue() {
    this.blockUIRevenue.start("Loading..");

    setTimeout(() => {
      this.blockUIRevenue.stop();
    }, 2500);
  }

  reloadHitRate() {
    this.blockUIHitRate.start("Please Wait..");

    setTimeout(() => {
      this.blockUIHitRate.stop();
    }, 2500);
  }

  reloadEmail() {
    this.blockUIEmail.start();

    setTimeout(() => {
      this.blockUIEmail.stop();
    }, 2500);
  }
  rotueInvoice() {
    this.route.navigate(["/invoice/invoice-summary"]);
  }
  getChartdata() {
    const Chartdata = this.salesData;
    this.lineArea = {
      type: "Line",
      data: Chartdata["lineArea"],
      options: {
        lineSmooth: Chartist.Interpolation.simple({
          divisor: 2.8,
        }),
        fullWidth: true,
        height: "270px",
        showArea: false,
        showPoint: false,
        axisX: {
          showGrid: false,
          showLabel: true,
          offset: 32,
        },
        axisY: {
          showGrid: true,
          showLabel: true,
          scaleMinSpace: 28,
          offset: 44,
        },
      },
      events: {
        created(data: any): void {
          const defs = data.svg.elem("defs");
          defs
            .elem("linearGradient", {
              id: "gradient2",
              x1: 0,
              y1: 0,
              x2: 1,
              y2: 0,
            })
            .elem("stop", {
              offset: 0,
              "stop-color": "rgb(255,73,97)",
            })
            .parent()
            .elem("stop", {
              offset: 1,
              "stop-color": "rgb(255,73,97)",
            });
        },
        draw(data: any): void {
          const circleRadius = 4;
          if (data.type === "point") {
            const circle = new Chartist.Svg("circle", {
              cx: data.x,
              cy: data.y,
              r: circleRadius,
              class: "ct-point-circle",
            });
            data.element.replace(circle);
          } else if (data.type === "label") {
            // adjust label position for rotation
            const dX = data.width / 2 + (26 - data.width);
            data.element.attr({ x: data.element.attr("x") - dX });
          }
        },
      },
    };
    this.earningchart = {
      type: "Line",
      data: Chartdata["earningchart"],
      options: {
        chartPadding: 0,
        height: "440px",
        low: 0,
        showArea: true,
        fullWidth: true,
        onlyInteger: true,
        axisX: {
          showGrid: false,
          showLabel: false,
          offset: -1,
        },
        axisY: {
          scaleMinSpace: 40,
          showGrid: false,
          showLabel: false,
          offset: -2,
        },
      },
      responsiveOptions: [
        [
          "screen and (max-width: 640px) and (min-width: 381px)",
          {
            axisX: {
              labelInterpolationFnc: function (value, index) {
                return index % 2 === 0 ? value : null;
              },
            },
          },
        ],
        [
          "screen and (max-width: 380px)",
          {
            axisX: {
              labelInterpolationFnc: function (value, index) {
                return index % 3 === 0 ? value : null;
              },
            },
          },
        ],
      ],
      events: {
        created(data: any): void {
          const defs = data.svg.elem("defs");
          defs
            .elem("linearGradient", {
              id: "gradient1",
              x1: 0,
              y1: 0,
              x2: 1,
              y2: 0,
            })
            .elem("stop", {
              offset: 0,
              "stop-color": "rgb(255,73,97)",
            })
            .parent()
            .elem("stop", {
              offset: 1,
              "stop-color": "rgb(255,73,97)",
            });
        },
        draw(data: any): void {
          const circleRadius = 6;
          if (data.type === "point") {
            const circle = new Chartist.Svg("circle", {
              cx: data.x,
              cy: data.y,
              r: circleRadius,
              class: "ct-point-circle",
            });
            data.element.replace(circle);
          }
        },
      },
    };
    // Doughnut
    this.donutChart2 = {
      type: "Pie",
      data: Chartdata["donut1"],
      options: {
        chartPadding: 0,
        fullwidth: true,
        height: "273px",
        donut: true,
        showLabel: true,
        startAngle: 0,
        labelInterpolationFnc: function (value) {
          const total = 82;
          return total + "%";
        },
      },
      events: {
        draw(data: any): void {
          if (data.type === "label") {
            if (data.index === 0) {
              data.element.attr({
                dx: data.element.root().width() / 2,
                dy: data.element.root().height() / 2,
              });
            } else {
              data.element.remove();
            }
          }
        },
      },
    };
    this.donutChart1 = {
      type: "Pie",
      data: Chartdata["donut2"],
      options: {
        chartPadding: 0,
        fullwidth: true,
        height: "273px",
        donut: true,
        showLabel: true,
        labelInterpolationFnc: function (value) {
          const total = 76;
          return total + "%";
        },
      },
      events: {
        draw(data: any): void {
          if (data.type === "label") {
            if (data.index === 0) {
              data.element.attr({
                dx: data.element.root().width() / 2,
                dy: data.element.root().height() / 2,
              });
            } else {
              data.element.remove();
            }
          }
        },
      },
    };
  }

  getCurrentStatus() {
    const apiParam = {
      tabIddepth: "Depth",
      tabIdsales: "Sales",
    };
    this.dashboardservice
      .GetCurrentStatus(apiParam)
      .subscribe((result: any) => {
        if (result.success) {
          this.singleSelectArray = result.data;
        }
      });
  }
  currentStatusDropdwon(cStatus) {
    this.getdashboarddata(cStatus);
  }
  getdashboarddata(c_Statsu = "") {
    // this.selectedChannelDatalist = this.headerService.selectedChannelDatalist;
    // this.selectedpartnerDatalist = this.headerService.selectedpartnerDatalist;
    // this.selectedregionDatalist = this.headerService.selectedregionDatalist;
    // this.selectedStateDatalist = this.headerService.selectedStateDatalist;
    // this.selectedLocationDatalist = this.headerService.selectedLocationDatalist;
    // this.selectedStoreCodeDatalist = this.headerService.selectedStoreCodeDatalist;
    // this.selectedBrandsDatalist = this.headerService.selectedBrandsDatalist;

    // this.productBrandDataDatalist = this.headerService.productBrandDataDatalist;
    // this.genderDatalist = this.headerService.genderDatalist;
    // this.categoryDataList = this.headerService.categoryDataList;
    // this.subCategoryDatalist = this.headerService.subCategoryDatalist;
    // this.typeDatalist = this.headerService.typeDatalist;
    // this.lastNameDatalist = this.headerService.lastNameDatalist;
    // this.seasonDatalist = this.headerService.seasonDatalist;
    // this.mrpDatalist = this.headerService.mrpDatalist;
    // this.articalNumberDatalist = this.headerService.articalNumberDatalist;

    // const apiParam = {
    //   channel: this.headerService.selectedChannelDatalist.join(","),
    //   partners: this.headerService.selectedpartnerDatalist.join(","),
    //   region: this.headerService.selectedregionDatalist.join(","),
    //   state: this.headerService.selectedStateDatalist.join(","),
    //   city: this.headerService.selectedLocationDatalist.join(","),
    //   storecode: this.headerService.selectedStoreCodeDatalist.join(","),
    //   locationbrand: this.headerService.selectedBrandsDatalist.join(","),
    //   productbrand: this.headerService.productBrandDataDatalist.join(","),
    //   gender: this.headerService.genderDatalist.join(","),
    //   category: this.headerService.categoryDataList.join(","),
    //   subcategory: this.headerService.subCategoryDatalist.join(","),
    //   type: this.headerService.typeDatalist.join(","),
    //   lastname: this.headerService.lastNameDatalist.join(","),
    //   season: this.headerService.seasonDatalist.join(","),
    //   mrp: this.headerService.mrpDatalist.join(","),
    //   articalno: this.headerService.articalNumberDatalist.join(","),
    //   other: this.singlebasicSelected,
    //   tabIddepth: "Depth",
    //   tabIdsales: "Sales",
    // };
    if (localStorage.getItem("filterJson")) {
      this.filterJson = localStorage.getItem("filterJson");
      var filterJson = JSON.parse(this.filterJson);
      //Product
      if (filterJson["productbrand"] != "") {
        this.productBrandDataDatalist = filterJson["productbrand"].split(",");
      }
      if (filterJson["gender"] != "") {
        this.genderDatalist = filterJson["gender"].split(",");
      }
      if (filterJson["category"] != "") {
        this.categoryDataList = filterJson["category"].split(",");
      }
      if (filterJson["subcategory"] != "") {
        this.subCategoryDatalist = filterJson["subcategory"].split(",");
      }
      if (filterJson["type"] != "") {
        this.typeDatalist = filterJson["type"].split(",");
      }
      if (filterJson["lastname"] != "") {
        this.lastNameDatalist = filterJson["lastname"].split(",");
      }
      if (filterJson["season"] != "") {
        this.seasonDatalist = filterJson["season"].split(",");
      }
      if (filterJson["mrp"] != "") {
        this.mrpDatalist = filterJson["mrp"].split(",");
      }
      if (filterJson["articalno"] != "") {
        this.articalNumberDatalist = filterJson["articalno"].split(",");
      }

      //Location
      if (filterJson["channel"] != "") {
        this.selectedChannelDatalist = filterJson["channel"].split(",");
      }
      if (filterJson["partners"] != "") {
        this.selectedpartnerDatalist = filterJson["partners"].split(",");
      }
      if (filterJson["region"] != "") {
        this.selectedregionDatalist = filterJson["region"].split(",");
      }
      if (filterJson["state"] != "") {
        this.selectedStateDatalist = filterJson["state"].split(",");
      }
      if (filterJson["city"] != "") {
        this.selectedLocationDatalist = filterJson["city"].split(",");
      }
      if (filterJson["locationbrand"] != "") {
        this.selectedBrandsDatalist = filterJson["locationbrand"].split(",");
      }
    } else {
      this.filterJson = "";
    }
    var params = {
      tabIddepth: this.typeDepth,
      tabIdsales: this.typeSales,
    };
    if (localStorage.getItem("filterJson")) {
      params = JSON.parse(localStorage.getItem("filterJson"));
      params["tabIddepth"] = this.typeDepth;
      params["tabIdsales"] = this.typeSales;
    }
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];

    this.dashboardservice.getdashboarddata(params).subscribe((result: any) => {
      if (result.success === 1) {
        this.dashboarddata = result.data;
        this.dashboardservice.pendingDecisions.next(
          result.data.pendingDecisions
        );

        this.currentstatussummaryDepth = result.data.currentstatussummaryDepth;
        this.currentstatussummarysales = result.data.currentstatussummarysales;
        const brandsCode = [];
        const dataData = [];

        result.data.chartsales.forEach(function (task) {
          brandsCode.push(parseInt(task.currtoday));
          dataData.push(
            String(new Date(task.currvalue).getDate()) +
              String(monthNames[new Date(task.currvalue).getMonth()])
          );
        });
        var blueData = [];
        var greenData = [];
        var yellowData = [];
        var redData = [];
        var blackData = [];
        this.lineChartsData = [];
        this.lineChartsLabels = ["23 Nov", "23 Dec"];
        result.data.chartDepth.map((item) => {
          if (item.colorId == "Blue") {
            blueData.push(parseInt(item.currtoday));
          } else if (item.colorId == "Green") {
            greenData.push(parseInt(item.currtoday));
          } else if (item.colorId == "Yellow") {
            yellowData.push(parseInt(item.currtoday));
          } else if (item.colorId == "Red") {
            redData.push(parseInt(item.currtoday));
          } else if (item.colorId == "Black") {
            blackData.push(parseInt(item.currtoday));
          }
        });

        this.lineChartsData = [
          { data: blueData, label: "" },
          { data: greenData, label: "" },
          { data: yellowData, label: "" },
          { data: redData, label: "" },
          { data: blackData, label: "" },
        ];

        this.lineChartsData2 = [
          {
            data: brandsCode,
            label: "",
          },
        ];
        this.lineChartsLabels2 = dataData;
      }
    });
  }

  changeDepthTab(type) {
    this.typeDepth = type;
    this.getdashboarddata();
  }

  changeSalesTab(type) {
    this.typeSales = type;
    this.currentstatussummarysales = [];
    this.getdashboarddata();
  }
}
// lineChartsData//

export const lineChartsData2: Array<any> = [
  {
    data: [1500, 1000, 1200, 1300, 1500, 1100, 900, 1100, 1000, 1500],
    label: "",
  },
];
export const lineChartsLabels: Array<any> = [
  "1 Jan",
  "1 Feb",
  "1 March",
  "1 April",
  "1 May",
];
export const lineChartsLabels2: Array<any> = [
  "1 Jan",
  "11 Jan",
  "21 Jan",
  "27 Jan",
  "1 Feb",
  "11 Feb",
  "21 Feb",
  "25 Feb",
  "27 Feb",
  "29 Feb",
];

export const lineChartsOptions: any = {
  animation: {
    duration: 1000, // general animation time
    easing: "easeOutBack",
  },
  hover: {
    animationDuration: 1000, // duration of animations when hovering an item
    mode: "label",
  },
  responsiveAnimationDuration: 1000, // animation duration after a resize
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    position: "right",
  },
  scales: {
    xAxes: [
      {
        display: true,
        gridLines: {
          color: "transparent",
          drawTicks: false,
        },
        ticks: {
          padding: 4,
        },
        scaleLabel: {
          display: true,
          labelString: "Month",
          color: "#636161",
        },
      },
    ],
    yAxes: [
      {
        display: true,
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        ticks: {
          padding: 4,
        },
        scaleLabel: {
          display: true,
          labelString: "Value",
          color: "#636161",
        },
      },
    ],
  },
  title: {
    display: true,
    text: "",
  },
};
export const lineChartsColors: Array<any> = [
  {
    fill: false,
    borderColor: "#0000ff",
    pointBorderColor: "#0000ff",
    pointBackgroundColor: "#0000ff",
    pointBorderWidth: 0,
    pointHoverBorderWidth: 0,
    pointRadius: 0,
  },
  {
    fill: false,
    borderColor: "#00ff00",
    pointBorderColor: "#00ff00",
    pointBackgroundColor: "#00ff00",
    pointBorderWidth: 0,
    pointHoverBorderWidth: 0,
    pointRadius: 0,
  },
  {
    fill: false,
    borderColor: "#ffff00",
    pointBorderColor: "#ffff00",
    pointBackgroundColor: "#ffff00",
    pointBorderWidth: 0,
    pointHoverBorderWidth: 0,
    pointRadius: 0,
  },
  {
    fill: false,
    borderColor: "#ff0000",
    pointBorderColor: "#ff0000",
    pointBackgroundColor: "#ff0000",
    pointBorderWidth: 0,
    pointHoverBorderWidth: 0,
    pointRadius: 0,
  },
  {
    fill: false,
    borderColor: " #4d4d4d",
    pointBorderColor: " #4d4d4d",
    pointBackgroundColor: " #4d4d4d",
    pointBorderWidth: 0,
    pointHoverBorderWidth: 0,
    pointRadius: 0,
  },
];
export const lineChartsLegend = true;
export const lineChartsType = "line";

// lineChartsData//
