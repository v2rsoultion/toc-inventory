import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  NgbCalendar,
  NgbDate,
  NgbDateStruct,
} from "@ng-bootstrap/ng-bootstrap";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import * as moment from "moment";
const selectData = require("../../../assets/data/forms/form-elements/select.json");
const now = new Date();
const I18N_VALUES = {
  en: {
    weekdays: ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
    months: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
  },
};

// Range datepicker Start
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one &&
  two &&
  two.year === one.year &&
  two.month === one.month &&
  two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
    ? one.month === two.month
      ? one.day === two.day
        ? false
        : one.day < two.day
      : one.month < two.month
    : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
    ? one.month === two.month
      ? one.day === two.day
        ? false
        : one.day > two.day
      : one.month > two.month
    : one.year > two.year;
// Range datepicker Ends

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"],
})
export class ReportComponent implements OnInit {
  public bottomEntriesArray = selectData.bottomEntriesArray;
  public breadcrumb: any;
  @BlockUI("numberTabs") blockUINumberTabs: NgBlockUI;
  @BlockUI("simpleDatepicker") blockUISimpleDatepicker: NgBlockUI;
  options = {
    minimize: true,
    reload: true,
    expand: true,
    close: true,
  };
  // Filters
  disabledD = false;
  ShowFilter = true;
  limitSelection = false;
  channelData = [];
  partnerData = [];
  stLocationData = [];
  stNameData = [];
  brandData = [];
  stCodeData = [];
  productdtData = [];
  productdtData1 = [];
  selectedItems = [];
  columnsData = [];
  selectedColumnsData: Array<any> = [];
  pdBrandData = [];
  pdGenderData = [];
  pdCtData = [];
  pdSbCtgData = [];
  pdTypeData = [];
  pdltNameData = [];
  pdSeasonData = [];
  pdMrpData = [];
  pdArtNoData = [];
  dropdownSettings: any = {};
  numberTab: FormGroup;
  stepOneForm: FormGroup;
  stepTwoForm: FormGroup;
  stepThreeForm: FormGroup;
  stepFourForm: FormGroup;
  isStepFourReached = false;
  isStepThreeReached = false;
  isStepTwoReached = false;
  isStepOneReached = false;
  selectedFromDate: any;
  selectedToDate: any;
  // datePicker Variable declaration
  d: any;
  d2: any;
  d3: any;
  model: NgbDateStruct;
  reportToModel;
  reportFromModel: any;
  date: { year: number; month: number };
  displayMonths = 2;
  navigation = "select";
  disabledModel: NgbDateStruct = {
    year: now.getFullYear(),
    month: now.getMonth() + 1,
    day: now.getDate(),
  };
  disabled = true;
  customModel: NgbDateStruct;
  configModal; // Global configuration of datepickers
  config: any = {};
  minDate: any;
  // Range datepicker start
  hoveredDate: NgbDateStruct;
  selectedDate: any;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  moment: any = moment;
  datePickerConfig: any = {
    format: "DD-MM-YYYY",
    max: moment(),
  };
  toDatePickerConfig: any = {
    format: "DD-MM-YYYY",
  };
  // Range datepicker starts
  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered = (date) =>
    this.fromDate &&
    !this.toDate &&
    this.hoveredDate &&
    after(date, this.fromDate) &&
    before(date, this.hoveredDate);
  isInside = (date) => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = (date) => equals(date, this.fromDate);
  isTo = (date) => equals(date, this.toDate);
  // Range datepicker ends

  // Selects today's date
  selectToday() {
    this.model = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate(),
    };
  }

  isDisabled(date: NgbDateStruct, current: { month: number }) {
    return date.month !== current.month;
  }
  // Custom Day View Ends

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      date.equals(this.toDate) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  constructor(
    private formBuilder: FormBuilder,
    private calendar: NgbCalendar
  ) {}

  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_id",
      textField: "item_text",
      selectAllText: " All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter,
    };

    // Product Filters
    this.pdBrandData = [
      { item_id: 1, item_text: "Brand1" },
      { item_id: 2, item_text: "Brand2" },
      { item_id: 3, item_text: "Brand3" },
      { item_id: 4, item_text: "Brand4" },
      { item_id: 5, item_text: "Brand5" },
      { item_id: 6, item_text: "Brand6" },
    ];
    this.pdGenderData = [
      { item_id: 1, item_text: "Male" },
      { item_id: 2, item_text: "Female" },
    ];
    this.pdCtData = [
      { item_id: 1, item_text: "Category1" },
      { item_id: 2, item_text: "Category2" },
      { item_id: 3, item_text: "Category3" },
      { item_id: 4, item_text: "Category4" },
      { item_id: 5, item_text: "Category5" },
      { item_id: 6, item_text: "Category6" },
    ];
    this.pdSbCtgData = [
      { item_id: 1, item_text: "Sub Category1" },
      { item_id: 2, item_text: "Sub Category2" },
      { item_id: 3, item_text: "Sub Category3" },
      { item_id: 4, item_text: "Sub Category4" },
      { item_id: 5, item_text: "Sub Category5" },
      { item_id: 6, item_text: "Sub Category6" },
    ];
    this.pdTypeData = [
      { item_id: 1, item_text: "Type1" },
      { item_id: 2, item_text: "Type2" },
      { item_id: 3, item_text: "Type3" },
      { item_id: 4, item_text: "Type4" },
      { item_id: 5, item_text: "Type5" },
      { item_id: 6, item_text: "Type6" },
    ];
    this.pdltNameData = [
      { item_id: 1, item_text: "Last Name1" },
      { item_id: 2, item_text: "Last Name2" },
      { item_id: 3, item_text: "Last Name3" },
      { item_id: 4, item_text: "Last Name4" },
      { item_id: 5, item_text: "Last Name5" },
      { item_id: 6, item_text: "Last Name6" },
    ];
    this.pdSeasonData = [
      { item_id: 1, item_text: "Season1" },
      { item_id: 2, item_text: "Season2" },
      { item_id: 3, item_text: "Season3" },
      { item_id: 4, item_text: "Season4" },
      { item_id: 5, item_text: "Season5" },
      { item_id: 6, item_text: "Season6" },
    ];
    this.pdMrpData = [
      { item_id: 1, item_text: "MRP1" },
      { item_id: 2, item_text: "MRP2" },
      { item_id: 3, item_text: "MRP3" },
      { item_id: 4, item_text: "MRP4" },
      { item_id: 5, item_text: "MRP5" },
      { item_id: 6, item_text: "MRP6" },
    ];
    this.pdArtNoData = [
      { item_id: 1, item_text: "Article1" },
      { item_id: 2, item_text: "Article2" },
      { item_id: 3, item_text: "Article3" },
      { item_id: 4, item_text: "Article4" },
      { item_id: 5, item_text: "Article5" },
      { item_id: 6, item_text: "Article6" },
    ];

    // Location Filters
    this.channelData = [
      { item_id: 1, item_text: "Channel1" },
      { item_id: 2, item_text: "Channel2" },
      { item_id: 3, item_text: "Channel3" },
      { item_id: 4, item_text: "Channel4" },
      { item_id: 5, item_text: "Channel5" },
      { item_id: 6, item_text: "Channel6" },
    ];
    this.partnerData = [
      { item_id: 1, item_text: "partner1" },
      { item_id: 2, item_text: "partner2" },
      { item_id: 3, item_text: "partner3" },
      { item_id: 4, item_text: "partner4" },
      { item_id: 5, item_text: "partner5" },
      { item_id: 6, item_text: "partner6" },
    ];
    this.stLocationData = [
      { item_id: 1, item_text: "location1" },
      { item_id: 2, item_text: "location2" },
      { item_id: 3, item_text: "location3" },
      { item_id: 4, item_text: "location4" },
      { item_id: 5, item_text: "location5" },
      { item_id: 6, item_text: "location6" },
    ];
    this.stNameData = [
      { item_id: 1, item_text: "Store Name1" },
      { item_id: 2, item_text: "Store Name2" },
      { item_id: 3, item_text: "Store Name3" },
      { item_id: 4, item_text: "Store Name4" },
      { item_id: 5, item_text: "Store Name5" },
      { item_id: 6, item_text: "Store Name6" },
    ];
    this.brandData = [
      { item_id: 1, item_text: "Brand1" },
      { item_id: 2, item_text: "Brand2" },
      { item_id: 3, item_text: "Brand3" },
      { item_id: 4, item_text: "Brand4" },
      { item_id: 5, item_text: "Brand5" },
      { item_id: 6, item_text: "Brand6" },
    ];
    this.stCodeData = [
      { item_id: 1, item_text: "Store Code1" },
      { item_id: 2, item_text: "Store Code2" },
      { item_id: 3, item_text: "Store Code3" },
      { item_id: 4, item_text: "Store Code4" },
      { item_id: 5, item_text: "Store Code5" },
      { item_id: 6, item_text: "Store Code6" },
    ];
    this.productdtData = [
      { item_id: 1, item_text: "Product data1" },
      { item_id: 2, item_text: "Product data2" },
      { item_id: 3, item_text: "Product data3" },
      { item_id: 4, item_text: "Product data4" },
      { item_id: 5, item_text: "Product data5" },
      { item_id: 6, item_text: "Product data6" },
    ];
    this.productdtData1 = [
      { item_id: 1, item_text: "Product data7" },
      { item_id: 2, item_text: "Product data8" },
      { item_id: 3, item_text: "Product data9" },
      { item_id: 4, item_text: "Product data0" },
      { item_id: 5, item_text: "Product data1" },
      { item_id: 6, item_text: "Product data2" },
    ];
    this.selectedColumnsData = [
      { item_id: 1, item_text: "Status" },
      { item_id: 2, item_text: "New Buffer Size" },
      { item_id: 3, item_text: "Buffer Size" },
      { item_id: 4, item_text: "SKU" },
      { item_id: 5, item_text: "SKU Description" },
      { item_id: 6, item_text: "DBM Policy" },
      { item_id: 7, item_text: "Company" },
    ];
    this.columnsData = [
      { item_id: 1, item_text: "Status" },
      { item_id: 2, item_text: "New Buffer Size" },
      { item_id: 3, item_text: "Buffer Size" },
      { item_id: 4, item_text: "SKU" },
      { item_id: 5, item_text: "SKU Description" },
      { item_id: 6, item_text: "DBM Policy" },
      { item_id: 7, item_text: "Company" },
      { item_id: 8, item_text: "Ean" },
      { item_id: 9, item_text: "New Article No" },
      { item_id: 10, item_text: "Article 9" },
      { item_id: 11, item_text: "Brand Name" },
      { item_id: 12, item_text: "Gender" },
      { item_id: 13, item_text: "Gender Code" },
      { item_id: 14, item_text: "Category" },
      { item_id: 15, item_text: "Category Code" },
      { item_id: 16, item_text: "Sub Category" },
      { item_id: 17, item_text: "Subcategory Code" },
      { item_id: 18, item_text: "Type" },
      { item_id: 19, item_text: "Type Code" },
      { item_id: 20, item_text: "Cut" },
      { item_id: 21, item_text: "Cut Code" },
      { item_id: 22, item_text: "Style" },
      { item_id: 23, item_text: "Style Code" },
      { item_id: 24, item_text: "Size" },
      { item_id: 25, item_text: "Vendor Code" },
      { item_id: 26, item_text: "Vendor Name" },
      { item_id: 27, item_text: "UPC" },
      { item_id: 28, item_text: "Landed Cost" },
      { item_id: 29, item_text: "Active Inactive Expired" },
      { item_id: 30, item_text: "Basic Upper Colour" },
      { item_id: 31, item_text: "Colour Code" },
      { item_id: 32, item_text: "User Edited by" },
      { item_id: 33, item_text: "Create Dt" },
      { item_id: 34, item_text: "Design Ref No" },
      { item_id: 35, item_text: "Design Code" },
      { item_id: 36, item_text: "Group Name" },
      { item_id: 37, item_text: "Is Active" },
      { item_id: 38, item_text: "Last No" },
      { item_id: 39, item_text: "Last Name" },
      { item_id: 40, item_text: "MUDA" },
      { item_id: 41, item_text: "Mutation" },
      { item_id: 42, item_text: "New Group" },
      { item_id: 43, item_text: "Prod Type Dec" },
      { item_id: 44, item_text: "Product Category" },
      { item_id: 45, item_text: "Shade" },
      { item_id: 46, item_text: "New Style Code" },
      { item_id: 47, item_text: "Sub Brand" },
      { item_id: 48, item_text: "Basic Upper Material" },
      { item_id: 49, item_text: "Sole Material" },
      { item_id: 50, item_text: "Trading Non Trading" },
      { item_id: 51, item_text: "Sale UOM" },
      { item_id: 52, item_text: "MRP" },
      { item_id: 53, item_text: "Long Description" },
      { item_id: 54, item_text: "Short Description" },
      { item_id: 55, item_text: "Size French" },
      { item_id: 56, item_text: "Size UK" },
      { item_id: 57, item_text: "Size CMS" },
      { item_id: 58, item_text: "HSN" },
      { item_id: 59, item_text: "Bar code setup" },
      { item_id: 60, item_text: "Alternate product code" },
      { item_id: 61, item_text: "Design Ref No1" },
      { item_id: 62, item_text: "AG Code" },
    ];

    this.selectToday();
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Report",
          isLink: false,
          link: "/report",
        },
      ],
    };
    this.numberTab = this.formBuilder.group({
      columns: [this.selectedColumnsData, Validators.required],
    });
  }
  public previousFourthStep() {
    this.isStepFourReached = true;
  }
  get f() {
    return this.stepOneForm.controls;
  }
  get i() {
    return this.stepTwoForm.controls;
  }
  get j() {
    return this.stepThreeForm.controls;
  }

  submit() {
    // window.alert("Download Report");
  }
  reloadSimpleDatepicker() {
    this.blockUISimpleDatepicker.start("Loading..");

    setTimeout(() => {
      this.blockUISimpleDatepicker.stop();
    }, 2500);
  }
  toogleShowFilter() {
    this.ShowFilter = !this.ShowFilter;
    this.dropdownSettings = Object.assign({}, this.dropdownSettings, {
      allowSearchFilter: this.ShowFilter,
    });
  }

  handleLimitSelection() {
    if (this.limitSelection) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, {
        limitSelection: 2,
      });
    } else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, {
        limitSelection: null,
      });
    }
  }

  changeMinDateTo(e) {
    delete this.toDatePickerConfig["min"];
    this.minDate = moment(e.date).format("YYYY-MM-DD");
    setTimeout(() => {
      this.toDatePickerConfig["min"] = moment(e.date);
    }, 100);
  }

  changeColumnData(event) {}

  removeColumns(i) {
    this.numberTab.value["columns"].splice(i, 1);
    this.numberTab.controls["columns"].setValue(
      this.numberTab.value["columns"]
    );
  }
}
