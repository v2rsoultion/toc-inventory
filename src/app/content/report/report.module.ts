import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReportComponent } from "./report.component";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { UiSwitchModule } from "ngx-ui-switch";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ChartistModule } from "ng-chartist";
import { ArchwizardModule } from "angular-archwizard";
import { MatchHeightModule } from "../partials/general/match-height/match-height.module";
import { CardModule } from "../partials/general/card/card.module";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { DpDatePickerModule } from "ng2-date-picker";
@NgModule({
  declarations: [ReportComponent],
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    BreadcrumbModule,
    CardModule,
    UiSwitchModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgMultiSelectDropDownModule,
    MatchHeightModule,
    NgbModule,
    ReactiveFormsModule,
    ArchwizardModule,
    DpDatePickerModule,
    RouterModule.forChild([
      {
        path: "",
        component: ReportComponent,
      },
    ]),
  ],
})
export class ReportModule {}
