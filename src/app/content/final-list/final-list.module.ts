import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FinalListComponent } from "./final-list.component";
import { ChartistModule } from "ng-chartist";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";
import { UiSwitchModule } from "ngx-ui-switch";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { RecordsTableModule } from "src/app/_layout/records-table/records-table.module";

@NgModule({
  declarations: [FinalListComponent],
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    ChartsModule,
    BreadcrumbModule,
    UiSwitchModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgbModule,
    RecordsTableModule,
    RouterModule.forChild([
      {
        path: "",
        component: FinalListComponent,
      },
    ]),
  ],
})
export class FinalListModule {}
