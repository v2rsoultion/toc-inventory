import { Component, OnInit, ViewChild } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { TableApiService } from "src/app/_services/table-api.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FinalService } from "src/app/_services/final.service";

@Component({
  selector: "app-final-list",
  templateUrl: "./final-list.component.html",
  styleUrls: ["./final-list.component.css"],
})
export class FinalListComponent implements OnInit {
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  rows: any;
  rowsJobs: any;
  editing = {};
  row: any;
  rowsFinalList: any;
  approvedCount: any;
  jobsData: any;

  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  @BlockUI("addRows") blockUIAddRows: NgBlockUI;

  columns: Array<any> = [
    { name: "SKU", status: true, element: "item" },
    { name: "From Location", status: true, element: "from_Location" },
    { name: "To Location", status: true, element: "to_Location" },
    { name: "Quantity", status: true, element: "qty" },
    { name: "Priority", status: true, element: "priority" },
    // { name: "Job Number", status: true, element: "store_site_buffer_Percent" },
    { name: "Comments", status: true, element: "comment" },
  ];
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal,
    private finalService: FinalService
  ) {}
  ngOnInit(): void {
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Final List",
          isLink: false,
          link: "/final-list",
        },
      ],
    };

    this.getFinalListData();
  }
  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }

  getFinalListData() {
    var params = {};
    this.finalService.getFinalList(params).subscribe((result: any) => {
      this.rows = result.data;
      this.approvedCount = this.rows.filter((item) => {
        return item.status == "Approved";
      }).length;
    });
  }
}
