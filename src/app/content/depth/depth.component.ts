import { Title } from "@angular/platform-browser";
import { filter } from "rxjs/operators";
import { DepthService } from "./../../_services/depth.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { HeaderfiltersService } from "src/app/_services/headerfilters.service";
import { TableApiService } from "src/app/_services/table-api.service";
const selectData = require("../../../assets//data/forms/form-elements/select.json");
import * as moment from "moment";
import { DashboardService } from "src/app/_services/dashboard.service";
@Component({
  selector: "app-depth",
  templateUrl: "./depth.component.html",
  styleUrls: ["./depth.component.css"],
})
export class DepthComponent implements OnInit {
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  options = {
    close: true,
    expand: true,
    minimize: true,
    reload: true,
  };
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  rows: any;
  editing = {};
  row: any;
  pendingdepthCount: any;
  filterJson: any = "";
  chartEvent: any;
  public columns = [
    { name: "Status", status: true, element: "status" },
    { name: "New Buffer Size", status: true, element: "newBufferSize" },
    { name: "Buffer Size", status: true, element: "buffer" },
    { name: "SKU", status: true, element: "sku" },
    { name: "DBM Policy", status: true, element: "dbM_Policy" },
    { name: "BP Site", status: true, element: "store_site_buffer_Percent" },
    { name: "BP Site Color", status: true, element: "store_site_buffer_color" },
    {
      name: "BP Virtual",
      status: true,
      element: "store_Virtul_buffer_Percent",
    },
    {
      name: "BP Virtual Color",
      status: true,
      element: "store_Virtul_buffer_color",
    },
    { name: "SKU Description", status: true, element: "sku_Desc" },
    { name: "Store Code", status: true, element: "store_Code" },
    { name: "Style Code", status: true, element: "stylecode" },
    { name: "Size French", status: true, element: "size_French" },
    { name: "MRP", status: true, element: "mrp" },
    { name: "Stock on Hand", status: true, element: "soh" },
  ];

  //Line Stacked Area Chart
  selectedChannelDatalist = [];
  selectedpartnerDatalist = [];
  selectedregionDatalist = [];
  selectedStateDatalist = [];
  selectedLocationDatalist = [];
  selectedStoreCodeDatalist = [];
  selectedBrandsDatalist = [];

  productBrandDataDatalist = [];
  genderDatalist = [];
  categoryDataList = [];
  subCategoryDatalist = [];
  typeDatalist = [];
  lastNameDatalist = [];
  seasonDatalist = [];
  mrpDatalist = [];
  articalNumberDatalist = [];
  singleSelectArray: Array<any> = [];
  lineChartData: Array<any> = [];
  lineChartLabels: Array<any> = [];
  // public lineChartData = lineChartData;

  // public lineChartLabels = lineChartLabels;
  public lineChartOptions = lineChartOptions;
  public lineChartColors = lineChartColors;
  public lineChartLegend = lineChartLegend;
  public lineChartType = lineChartType;

  // public singleSelectArray = selectData.singleSelectArray;
  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  @BlockUI("addRows") blockUIAddRows: NgBlockUI;
  @BlockUI("rowSelection") blockUIRowSelection: NgBlockUI;
  @BlockUI("basicProgress") blockUIBasicProgress: NgBlockUI;
  @BlockUI("coloredProgress") blockUIColoredProgress: NgBlockUI;
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal,
    private headerService: HeaderfiltersService,
    private depthService: DepthService,
    private titleService: Title,
    private dashboardservice: DashboardService
  ) {}
  ngOnInit(): void {
    this.titleService.setTitle("Depth | TOC-Inventory ");
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Depth",
          isLink: false,
          link: "/depth",
        },
      ],
    };
    // this.tableApiservice.getTableApiData().subscribe((Response) => {
    //   this.data = Response;
    //   this.getTabledata();
    // });
    this.getDepthdata();
    this.getCurrentStatus();
    this.headerService.callFilterDataRef.subscribe((res) => {
      if (res) {
        this.getDepthdata();
        this.headerService.callFilterData.next(false);
      }
    });
  }

  getCurrentStatus() {
    const apiParam = {
      tabIddepth: "Depth",
      tabIdsales: "Sales",
    };
    this.dashboardservice
      .GetCurrentStatus(apiParam)
      .subscribe((result: any) => {
        if (result.success) {
          this.singleSelectArray = result.data;
        }
      });
  }
  // getTabledata() {
  //   this.rows = this.data.rows;
  //   this.row = this.data.row;
  // }

  // addFieldValue() {
  //   this.rows.push(this.newAttribute);
  //   this.rows = [...this.rows];
  // }

  private newAttribute = {
    status: "",
    newBufferSize: "",
    bufferSize: "",
    sku: "",
    dbmPolicy: "",
    bp: "",
    skuDescription: "",
  };
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  chartModelLoad(content, event) {
    if (event && event != "") {
      this.lineChartLabels = [];
      this.chartEvent = event;
      var blackData = [];
      var redData = [];
      var yellowData = [];
      var greenData = [];
      var blueData = [];
      var inventoryData = [];
      this.depthService.getDepthChartData(event).subscribe((res) => {
        if (res["success"] == 1) {
          var data = res["data"];
          data.map((item, key) => {
            if (item["heading"] == "Black") {
              this.lineChartLabels.push(
                moment(item["monthdate"]).format("MMMYYYY")
              );
              blackData.push(item["valdata"]);
            }
            if (item["heading"] == "Red") {
              redData.push(item["valdata"]);
            }
            if (item["heading"] == "Yellow") {
              yellowData.push(item["valdata"]);
            }
            if (item["heading"] == "Green") {
              greenData.push(item["valdata"]);
            }
            if (item["heading"] == "Blue") {
              blueData.push(item["valdata"]);
            }
            if (item["heading"] == "Inventory") {
              inventoryData.push(item["valdata"]);
            }
            if (key == data.length - 1) {
              this.lineChartData = [
                { data: blackData, label: "Black" },
                { data: redData, label: "Red" },
                { data: yellowData, label: "Yellow" },
                { data: greenData, label: "Green" },
                { data: blueData, label: "Blue" },
                { data: inventoryData, label: "Inventory" },
              ];
              console.log(this.lineChartData);
              console.log(this.lineChartLabels);
              setTimeout(() => {
                this.modalService.open(content, {
                  windowClass: "animated fadeInDown",
                  size: "lg",
                });
              }, 1000);
            }
          });
        }
      });
    }
  }

  changeChartViewData(content) {
    if (this.chartEvent && this.chartEvent != "") {
      // this.modalService.dismissAll();
      this.lineChartLabels = [];
      var blackData = [];
      var redData = [];
      var yellowData = [];
      var greenData = [];
      var blueData = [];
      var inventoryData = [];
      this.depthService.getDepthChartData(this.chartEvent).subscribe((res) => {
        if (res["success"] == 1) {
          var data = res["data"];
          data.map((item, key) => {
            if (item["heading"] == "Black") {
              this.lineChartLabels.push(
                moment(item["monthdate"]).format("MMMYYYY")
              );
              blackData.push(item["valdata"]);
            }
            if (item["heading"] == "Red") {
              redData.push(item["valdata"]);
            }
            if (item["heading"] == "Yellow") {
              yellowData.push(item["valdata"]);
            }
            if (item["heading"] == "Green") {
              greenData.push(item["valdata"]);
            }
            if (item["heading"] == "Blue") {
              blueData.push(item["valdata"]);
            }
            if (item["heading"] == "Inventory") {
              inventoryData.push(item["valdata"]);
            }
            if (key == data.length - 1) {
              this.lineChartData = [
                { data: blackData, label: "Black" },
                { data: redData, label: "Red" },
                { data: yellowData, label: "Yellow" },
                { data: greenData, label: "Green" },
                { data: blueData, label: "Blue" },
                { data: inventoryData, label: "Inventory" },
              ];
              // this.modalService.open(content, {
              //   windowClass: "animated fadeInDown",
              //   size: "lg",
              // });
            }
          });
        }
      });
    }
  }

  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }
  SmallModel(SmallModelContent) {
    this.modalService.open(SmallModelContent, {
      windowClass: "animated fadeInDown",
      size: "sm",
    });
  }
  getDepthdata() {
    if (localStorage.getItem("filterJson")) {
      this.filterJson = localStorage.getItem("filterJson");
      var filterJson = JSON.parse(this.filterJson);
      //Product
      if (filterJson["productbrand"] != "") {
        this.productBrandDataDatalist = filterJson["productbrand"].split(",");
      }
      if (filterJson["gender"] != "") {
        this.genderDatalist = filterJson["gender"].split(",");
      }
      if (filterJson["category"] != "") {
        this.categoryDataList = filterJson["category"].split(",");
      }
      if (filterJson["subcategory"] != "") {
        this.subCategoryDatalist = filterJson["subcategory"].split(",");
      }
      if (filterJson["type"] != "") {
        this.typeDatalist = filterJson["type"].split(",");
      }
      if (filterJson["lastname"] != "") {
        this.lastNameDatalist = filterJson["lastname"].split(",");
      }
      if (filterJson["season"] != "") {
        this.seasonDatalist = filterJson["season"].split(",");
      }
      if (filterJson["mrp"] != "") {
        this.mrpDatalist = filterJson["mrp"].split(",");
      }
      if (filterJson["articalno"] != "") {
        this.articalNumberDatalist = filterJson["articalno"].split(",");
      }

      //Location
      if (filterJson["channel"] != "") {
        this.selectedChannelDatalist = filterJson["channel"].split(",");
      }
      if (filterJson["partners"] != "") {
        this.selectedpartnerDatalist = filterJson["partners"].split(",");
      }
      if (filterJson["region"] != "") {
        this.selectedregionDatalist = filterJson["region"].split(",");
      }
      if (filterJson["state"] != "") {
        this.selectedStateDatalist = filterJson["state"].split(",");
      }
      if (filterJson["city"] != "") {
        this.selectedLocationDatalist = filterJson["city"].split(",");
      }
      if (filterJson["locationbrand"] != "") {
        this.selectedBrandsDatalist = filterJson["locationbrand"].split(",");
      }
    } else {
      this.filterJson = "";
    }

    // this.selectedChannelDatalist = this.headerService.selectedChannelDatalist;
    // this.selectedpartnerDatalist = this.headerService.selectedpartnerDatalist;
    // this.selectedregionDatalist = this.headerService.selectedregionDatalist;
    // this.selectedStateDatalist = this.headerService.selectedStateDatalist;
    // this.selectedLocationDatalist = this.headerService.selectedLocationDatalist;
    // this.selectedStoreCodeDatalist = this.headerService.selectedStoreCodeDatalist;
    // this.selectedBrandsDatalist = this.headerService.selectedBrandsDatalist;

    // this.productBrandDataDatalist = this.headerService.productBrandDataDatalist;
    // this.genderDatalist = this.headerService.genderDatalist;
    // this.categoryDataList = this.headerService.categoryDataList;
    // this.subCategoryDatalist = this.headerService.subCategoryDatalist;
    // this.typeDatalist = this.headerService.typeDatalist;
    // this.lastNameDatalist = this.headerService.lastNameDatalist;
    // this.seasonDatalist = this.headerService.seasonDatalist;
    // this.mrpDatalist = this.headerService.mrpDatalist;
    // this.articalNumberDatalist = this.headerService.articalNumberDatalist;

    const brandsCode = [];

    this.headerService.selectedStoreCodeDatalist.forEach(function (task) {
      let n = task.substring(task.lastIndexOf("(") + 1, task.lastIndexOf(")"));
      brandsCode.push(n);
    });
    // const apiParam = {
    //   channel: this.headerService.selectedChannelDatalist.join(","),
    //   partners: this.headerService.selectedpartnerDatalist.join(","),
    //   region: this.headerService.selectedregionDatalist.join(","),
    //   state: this.headerService.selectedStateDatalist.join(","),
    //   city: this.headerService.selectedLocationDatalist.join(","),
    //   storecode: brandsCode.join(","),
    //   locationbrand: this.headerService.selectedBrandsDatalist.join(","),
    //   productbrand: this.headerService.productBrandDataDatalist.join(","),
    //   gender: this.headerService.genderDatalist.join(","),
    //   category: this.headerService.categoryDataList.join(","),
    //   subcategory: this.headerService.subCategoryDatalist.join(","),
    //   type: this.headerService.typeDatalist.join(","),
    //   lastname: this.headerService.lastNameDatalist.join(","),
    //   season: this.headerService.seasonDatalist.join(","),
    //   mrp: this.headerService.mrpDatalist.join(","),
    //   articalno: this.headerService.articalNumberDatalist.join(","),
    // };
    var params = {};
    if (localStorage.getItem("filterJson")) {
      params = JSON.parse(localStorage.getItem("filterJson"));
    }
    this.depthService.getDepthdata(params).subscribe((result: any) => {
      this.pendingdepthCount = result.data.depthPendingDecision["pendingcount"];
      this.rows = result.data.depthList;
    });
  }

  // getApproveRejectDepth(status){

  //   const apiParam = {
  //     Is_Approve:status,
  //     Ids:String(this.donerrowData.id),
  //     FinalRecommend:[]
  //   }
  //   this.ibttServices.getApproveRejectDepth(apiParam).subscribe((result: any) => {

  //     this.getIbttdata();
  //     this.lockedWindow.close();
  //   });
  // }

  clearAllFilter() {
    localStorage.removeItem("filterJson");
    localStorage.removeItem("filterId");
    this.getDepthdata();
    this.filterJson = "";
    this.headerService.callFilterData.next(true);
  }
}

// export const lineChartData: Array<any> = [
//   { data: [0, 10, 8, 17, 8, 15, 7], label: "Series A" },
//   { data: [10, 20, 15, 25, 19, 22, 18], label: "Series B" },
//   { data: [28, 48, 35, 29, 46, 27, 60], label: "Series C" },
//   { data: [56, 70, 55, 46, 67, 52, 70], label: "Series D" },
//   { data: [60, 75, 65, 56, 77, 62, 80], label: "Series E" },
//   { data: [45, 60, 10, 25, 90, 45, 60], label: "Inventory in Store" },
// ];
// export const lineChartLabels: Array<any> = [
//   "January",
//   "February",
//   "March",
//   "April",
//   "May",
//   "June",
//   "July",
// ];
export const lineChartOptions: any = {
  animation: {
    duration: 1000, // general animation timebar
    easing: "easeOutBack",
  },
  hover: {
    animationDuration: 1000, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 1000, // animation duration after a resize
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [
      {
        display: true,
        ticks: {
          padding: 4,
        },
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        scaleLabel: {
          display: true,
          labelString: "Month",
        },
      },
    ],
    yAxes: [
      {
        display: true,
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        ticks: {
          padding: 4,
        },
        scaleLabel: {
          display: true,
          labelString: "Value",
        },
      },
    ],
  },
};
export const lineChartColors: Array<any> = [
  {
    backgroundColor: "#4D4D4D",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "#FF0000",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "#FFFF00",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "#00FF00",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },

  {
    backgroundColor: "#0000FF",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "transparent",
    borderColor: "#b3b3b3",
    pointBackgroundColor: "#b3b3b3",
    pointBorderColor: "#b3b3b3",
  },
];
export const lineChartLegend = true;
export const lineChartType = "line";
